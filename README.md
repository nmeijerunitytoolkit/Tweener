# Tweener

### Description
An easy to use and expand tweener. Meant to help you easily animate any object.

### Usage
To animate any object simply add the `Tweener` component. From there add the needed `TweenerTrack` objects. Auto play can be enabled, or the tweener can be started with `Play()` from code.

To create a new tweener track simply create a new class that inherits from `TweenerTrack`. It requires that you implement the following functions:
* `Lerp()`: Set the current progress on the object this track changes
* `SetStart()`: Set the start value to the current value of the object this track changes (Used only in the editor)
* `SetEnd()`: Set the end value to the current value of the object this track changes (Used only in the editor)

### Installation
This package can be installed directly with Git. For more information: https://docs.unity3d.com/Manual/upm-ui-giturl.html