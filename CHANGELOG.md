# v1.0.1
* Fixed bug where the animation curve wasn't used for start/end

# v1.0.0
* Initial release