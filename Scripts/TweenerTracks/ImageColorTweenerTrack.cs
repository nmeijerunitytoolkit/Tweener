﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace NMeijer.Tweening
{
	[Serializable]
	public class ImageColorTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private Image _image = default;
		[SerializeField]
		private Color _fromColor = default;
		[SerializeField]
		private Color _toColor = default;

		#endregion

		#region Public Methods

		public override void SetStart()
		{
			_fromColor = _image.color;
		}

		public override void SetEnd()
		{
			_toColor = _image.color;
		}

		#endregion

		#region Protected Methods

		protected override void Lerp(float progress)
		{
			if(_image != null)
			{
				_image.color = Color.LerpUnclamped(_fromColor, _toColor, progress);
			}
		}

		#endregion
	}
}