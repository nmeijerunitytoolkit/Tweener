﻿using UnityEngine;

namespace NMeijer.Tweening
{
	public abstract class SpriteAnimationTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private Sprite[] _sprites = default;

		#endregion

		#region Variables

		private int _currentIndex = -1;
		private int _spriteLength;

		#endregion

		#region Public Methods

		public override void SetStart()
		{
			// Setting a start doesn't apply for this track
		}

		public override void SetEnd()
		{
			// Setting an end doesn't apply for this track
		}

		#endregion

		#region Protected Methods

		protected override void Lerp(float progress)
		{
			int newIndex = Mathf.RoundToInt((_sprites.Length - 1) * progress);
			if(newIndex != _currentIndex)
			{
				SetSprite(_sprites[newIndex]);
				_currentIndex = newIndex;
			}
		}

		protected abstract void SetSprite(Sprite sprite);

		#endregion
	}
}