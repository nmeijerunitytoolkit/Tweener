﻿using UnityEngine;

namespace NMeijer.Tweening
{
	public class SpriteRendererSpriteAnimationTweenerTrack : SpriteAnimationTweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private SpriteRenderer _spriteRenderer = default;

		#endregion

		#region Protected Methods

		protected override void SetSprite(Sprite sprite)
		{
			_spriteRenderer.sprite = sprite;
		}

		#endregion
	}
}