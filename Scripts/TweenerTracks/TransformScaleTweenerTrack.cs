﻿using UnityEngine;

namespace NMeijer.Tweening
{
	public class TransformScaleTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private Transform _transform = default;
		[SerializeField]
		private Vector3 _startPosition = default;
		[SerializeField]
		private Vector3 _endPosition = default;

		#endregion

		#region Public Methods

		public override void SetStart()
		{
			_startPosition = _transform.localScale;
		}

		public override void SetEnd()
		{
			_endPosition = _transform.localScale;
		}

		protected override void Lerp(float progress)
		{
			_transform.localScale = Vector3.LerpUnclamped(_startPosition, _endPosition, progress);
		}

		#endregion
	}
}