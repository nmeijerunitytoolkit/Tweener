﻿using System;
using UnityEngine;

namespace NMeijer.Tweening
{
	[Serializable]
	public class RectTransformTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private RectTransform _transform = default;
		[SerializeField]
		private Vector3 _startPosition = default;
		[SerializeField]
		private Vector3 _endPosition = default;

		#endregion

		#region Public Methods

		public override void SetStart()
		{
			_startPosition = _transform.anchoredPosition;
		}

		public override void SetEnd()
		{
			_endPosition = _transform.anchoredPosition;
		}

		#endregion

		#region Protected Methods

		protected override void Lerp(float progress)
		{
			if(_transform != null)
			{
				_transform.anchoredPosition = Vector3.LerpUnclamped(_startPosition, _endPosition, progress);
			}
		}

		#endregion
	}
}