﻿using System;
using System.Collections;
using UnityEngine;

namespace NMeijer.Tweening
{
	[Serializable]
	public abstract class TweenerTrack : MonoBehaviour
	{
		#region Editor Variables

		[SerializeField]
		private AnimationCurve _animationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
		[SerializeField]
		private float _startTime = default;
		[SerializeField]
		private float _endTime = 1;

		#endregion

		#region Variables

		private float _globalEndTime;

		private Coroutine _coroutine;

		#endregion

		#region Properties

		public float EndTime => _endTime;

		#endregion

		#region Public Methods

		internal void Initialize(float globalEndTime)
		{
			_globalEndTime = globalEndTime;
			Lerp(EvaluateCurve(0f));
		}

		internal void Play(bool reverse = false)
		{
			_coroutine = StartCoroutine(PlayRoutine(reverse));
		}

		internal void Stop()
		{
			StopCoroutine(_coroutine);
		}

		internal void Show(float progress)
		{
			float time = progress * _globalEndTime;
			if(time < _startTime)
			{
				Lerp(EvaluateCurve(0f));
			}
			else if(time > _endTime)
			{
				Lerp(EvaluateCurve(1f));
			}
			else
			{
				float val = (time - _startTime) / (_endTime - _startTime);
				Lerp(EvaluateCurve(float.IsNaN(val) ? 0f : val));
			}
		}

		#endregion

		#region Protected Methods

		protected abstract void Lerp(float progress);

		public abstract void SetStart();

		public abstract void SetEnd();

		#endregion

		#region Private Methods

		private IEnumerator PlayRoutine(bool reverse)
		{
			yield return new WaitForSeconds(reverse ? _globalEndTime - _endTime : _startTime);

			float timeToPlay = _endTime - _startTime;
			float time = 0;

			while(time < timeToPlay)
			{
				float passedTime = time / timeToPlay;
				float localProgress = reverse ? 1f - passedTime : passedTime;
				Lerp(EvaluateCurve(localProgress));
				time += Time.deltaTime;
				yield return null;
			}

			Lerp(EvaluateCurve(reverse ? 0f : 1f));
		}

		#endregion

		#region Private Methods

		private float EvaluateCurve(float progress)
		{
			return _animationCurve.Evaluate(progress);
		}

		#endregion

		#region Editor

#if UNITY_EDITOR
		private void OnValidate()
		{
			hideFlags = HideFlags.HideInInspector;
		}
#endif

		#endregion
	}
}