﻿using UnityEngine;
using UnityEngine.UI;

namespace NMeijer.Tweening
{
	public class TextColorTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private Text _text = default;
		[SerializeField]
		private Color _fromColor = default;
		[SerializeField]
		private Color _toColor = default;

		#endregion

		#region Public Methods

		public override void SetEnd()
		{
			_toColor = _text.color;
		}

		public override void SetStart()
		{
			_fromColor = _text.color;
		}

		#endregion

		#region Protected Methods

		protected override void Lerp(float progress)
		{
			_text.color = Color.LerpUnclamped(_fromColor, _toColor, progress);

#if UNITY_EDITOR
			if(!Application.isPlaying)
			{
				// Unity doesn't refresh Text components if changed out of PlayMode for some reason
				_text.enabled = false;
				_text.enabled = true;
			}
#endif
		}

		#endregion
	}
}