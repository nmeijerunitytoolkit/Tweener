﻿using UnityEngine;
using UnityEngine.UI;

namespace NMeijer.Tweening
{
	public class ImageSpriteAnimationTweenerTrack : SpriteAnimationTweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private Image _image = default;

		#endregion

		#region Protected Methods

		protected override void SetSprite(Sprite sprite)
		{
			_image.sprite = sprite;
		}

		#endregion
	}
}