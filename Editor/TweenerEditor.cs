﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace NMeijer.Tweening.Editor
{
	[CustomEditor(typeof(Tweener))]
	public class TweenerEditor : UnityEditor.Editor
	{
		#region Variables

		private bool _foldout = true;
		private float _progress;
		private bool _play;
		private bool _reverse;
		private double _lastFrameTime;

		private SerializedProperty _tweenerTracksProperty;
		private SerializedProperty _playOnAwakeProperty;
		private SerializedProperty _allowPlayingWhileBusyProperty;
		private SerializedProperty _loopProperty;
		private SerializedProperty _loopBackAndForthProperty;

		private GenericMenu _genericMenu;

		#endregion

		#region Properties

		private string EditorPrefsProgressKey => "Progress" + target.GetInstanceID();

		#endregion

		#region Lifecycle

		private void OnEnable()
		{
			_tweenerTracksProperty = serializedObject.FindProperty("_tweenerTracks");
			_playOnAwakeProperty = serializedObject.FindProperty("_playOnAwake");
			_allowPlayingWhileBusyProperty = serializedObject.FindProperty("_allowPlayingWhileBusy");
			_loopProperty = serializedObject.FindProperty("_loop");
			_loopBackAndForthProperty = serializedObject.FindProperty("_loopBackAndForth");

			_genericMenu = new GenericMenu();
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			for(int i = 0, iLength = assemblies.Length; i < iLength; i++)
			{
				Type[] types = assemblies[i].GetTypes();
				for(int j = 0, jLength = types.Length; j < jLength; j++)
				{
					Type type = types[j];
					if(type.IsSubclassOf(typeof(TweenerTrack)) && !type.IsAbstract)
					{
						_genericMenu.AddItem(new GUIContent(TweenerEditorUtility.GetReadableTweenerTrackName(type.Name)), false, () => AddTweenerTrack(type));
					}
				}
			}

			_progress = EditorPrefs.GetFloat(EditorPrefsProgressKey);
		}

		private void OnDisable()
		{
			if(target != null)
			{
				EditorPrefs.SetFloat(EditorPrefsProgressKey, _progress);
			}
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();

			Tweener tweener = (Tweener)target;
			_foldout = EditorGUILayout.Foldout(_foldout, "Tweener Tracks", true);
			if(_foldout)
			{
				int indexToDelete = -1;

				EditorGUI.indentLevel = 1;
				for(int i = 0, length = _tweenerTracksProperty.arraySize; i < length; i++)
				{
					GUILayout.BeginVertical("Box");

					TweenerTrack track = (TweenerTrack)_tweenerTracksProperty.GetArrayElementAtIndex(i).objectReferenceValue;
					SerializedObject obj = new SerializedObject(track);

					EditorGUILayout.LabelField(TweenerEditorUtility.GetReadableTweenerTrackName(track.GetType().Name), EditorStyles.boldLabel);

					EditorGUI.BeginChangeCheck();

					SerializedProperty iterator = obj.GetIterator();
					Undo.RecordObject(track, track.GetInstanceID().ToString());
					bool hasChildren = true;
					while(iterator.NextVisible(hasChildren))
					{
						EditorGUI.indentLevel = iterator.depth + 1;
						if(iterator.name != "x" && iterator.name != "y" && iterator.name != "z" && iterator.name != "m_Script")
						{
							EditorGUILayout.PropertyField(iterator);
						}
						hasChildren = iterator.hasChildren && iterator.isExpanded;
					}
					GUILayout.BeginHorizontal();
					if(GUILayout.Button("Set as start"))
					{
						track.SetStart();
					}
					if(GUILayout.Button("Set as end"))
					{
						track.SetEnd();
					}
					GUILayout.EndHorizontal();
					if(EditorGUI.EndChangeCheck())
					{
						obj.ApplyModifiedProperties();
					}

					if(GUILayout.Button("Delete"))
					{
						indexToDelete = i;
					}

					GUILayout.EndVertical();
					GUILayout.Space(5f);
				}

				if(indexToDelete >= 0)
				{
					SerializedProperty property = _tweenerTracksProperty.GetArrayElementAtIndex(indexToDelete);
					DestroyImmediate(property.objectReferenceValue);
					property.objectReferenceValue = null;
					_tweenerTracksProperty.DeleteArrayElementAtIndex(indexToDelete);
					serializedObject.ApplyModifiedProperties();
				}
			}
			EditorGUI.indentLevel = 0;

			GUILayout.BeginHorizontal();
			GUI.enabled = !_reverse;

			if(!_play)
			{
				if(GUILayout.Button("Play"))
				{
					tweener.EditorInitialize();
					_play = true;
					_progress = 0;
					_lastFrameTime = EditorApplication.timeSinceStartup;
					EditorApplication.update += PlayAnimation;
				}
			}
			else if(GUILayout.Button("Stop"))
			{
				_play = false;
				EditorApplication.update -= PlayAnimation;
			}

			GUI.enabled = !_play && !_reverse;

			EditorGUI.BeginChangeCheck();
			float newProgress = EditorGUILayout.Slider(_progress, 0, 1);

			if(!Mathf.Approximately(newProgress, _progress) && EditorGUI.EndChangeCheck())
			{
				tweener.EditorInitialize();
				for(int i = 0, length = tweener.EditorTweenerTracks.Length; i < length; i++)
				{
					tweener.EditorTweenerTracks[i].Show(newProgress);
				}
				_progress = newProgress;
			}

			GUI.enabled = !_play;

			if(!_reverse)
			{
				if(GUILayout.Button("Play reverse"))
				{
					tweener.EditorInitialize();
					_reverse = true;
					_progress = 1;
					_lastFrameTime = EditorApplication.timeSinceStartup;
					EditorApplication.update += PlayAnimation;
				}
			}
			else if(GUILayout.Button("Stop reverse"))
			{
				_reverse = false;
				EditorApplication.update -= PlayAnimation;
			}

			GUI.enabled = !_play;
			GUILayout.EndHorizontal();

			if(GUILayout.Button("Add Tweener Track"))
			{
				_genericMenu.ShowAsContext();
			}

			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Save as preset"))
			{
				string folder = EditorUtility.SaveFilePanelInProject("Animation preset", "Animation preset", "animPreset", "Save the animation preset");
				TweenerEditorUtility.SaveTweenerAsPreset(folder, tweener);
			}

			if(GUILayout.Button("Load preset"))
			{
				string folder = EditorUtility.OpenFilePanel("Animation preset", Application.dataPath, "animPreset");
				TweenerEditorUtility.LoadTweenerFromPreset(folder, tweener);
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			_allowPlayingWhileBusyProperty.boolValue = EditorGUILayout.Toggle("Allow Playing While Busy", _allowPlayingWhileBusyProperty.boolValue);
			_playOnAwakeProperty.boolValue = EditorGUILayout.Toggle("Play On Awake", _playOnAwakeProperty.boolValue);
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUI.BeginDisabledGroup(_loopBackAndForthProperty.boolValue);
			_loopProperty.boolValue = EditorGUILayout.Toggle("Loop", _loopProperty.boolValue);
			EditorGUI.EndDisabledGroup();

			EditorGUI.BeginDisabledGroup(_loopProperty.boolValue);
			_loopBackAndForthProperty.boolValue = EditorGUILayout.Toggle("Loop Back And Forth", _loopBackAndForthProperty.boolValue);
			EditorGUI.EndDisabledGroup();
			GUILayout.EndHorizontal();

			if(EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
			}
		}

		#endregion

		#region Private Methods

		private void PlayAnimation()
		{
			Tweener tweener = (Tweener)target;
			float time = _progress * tweener.EndTime;
			float timeDiff = (float)(EditorApplication.timeSinceStartup - _lastFrameTime);
			time += _reverse ? -timeDiff : timeDiff;
			_lastFrameTime = EditorApplication.timeSinceStartup;
			_progress = time / tweener.EndTime;

			for(int i = 0, length = tweener.EditorTweenerTracks.Length; i < length; i++)
			{
				tweener.EditorTweenerTracks[i].Show(_progress);
			}

			if((_play && _progress >= 1) || (_reverse && _progress <= 0))
			{
				_progress = _reverse ? 0f : 1f;
				_play = false;
				_reverse = false;
				EditorApplication.update -= PlayAnimation;
			}

			Repaint();
		}

		private void AddTweenerTrack(Type type)
		{
			TweenerTrack track = (TweenerTrack)((Tweener)target).gameObject.AddComponent(type);
			_tweenerTracksProperty.arraySize++;
			_tweenerTracksProperty.GetArrayElementAtIndex(_tweenerTracksProperty.arraySize - 1).objectReferenceValue = track;
			serializedObject.ApplyModifiedProperties();
		}

		#endregion
	}
}