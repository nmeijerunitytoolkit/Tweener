﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace NMeijer.Tweening.Editor
{
	public static class TweenerEditorUtility
	{
		#region Public Methods

		public static void SaveTweenerAsPreset(string filePath, Tweener tweener)
		{
			if(string.IsNullOrEmpty(filePath))
			{
				return;
			}

			int length = tweener.TweenerTracks.Length;
			TweenerTrackSaveContainer[] trackSaveContainers = new TweenerTrackSaveContainer[length];
			for(int i = 0; i < length; i++)
			{
				TweenerTrack track = tweener.TweenerTracks[i];
				Type trackType = track.GetType();
				trackSaveContainers[i] = new TweenerTrackSaveContainer(trackType.FullName, trackType.Assembly.FullName, JsonUtility.ToJson(track, true));
			}
			string json = JsonUtility.ToJson(new TweenerSaveContainer(trackSaveContainers), true);
			File.WriteAllText(filePath, json);
		}

		public static void LoadTweenerFromPreset(string filePath, Tweener tweener)
		{
			for(int i = 0, length = tweener.TweenerTracks.Length; i < length; i++)
			{
				Object.DestroyImmediate(tweener.TweenerTracks[i]);
			}

			SerializedObject serializedObject = new SerializedObject(tweener);
			SerializedProperty tweenerTracksProperty = serializedObject.FindProperty("_tweenerTracks");

			TweenerSaveContainer tweenerSaveContainer = JsonUtility.FromJson<TweenerSaveContainer>(File.ReadAllText(filePath));
			int newTracksLength = tweenerSaveContainer.TweenerTrackSaveContainers.Length;
			tweenerTracksProperty.arraySize = newTracksLength;
			for(int i = 0; i < newTracksLength; i++)
			{
				TweenerTrackSaveContainer trackSaveContainer = tweenerSaveContainer.TweenerTrackSaveContainers[i];
				TweenerTrack track = (TweenerTrack)tweener.gameObject.AddComponent(Type.GetType(string.Join(",", trackSaveContainer.ClassName, trackSaveContainer.Assembly)));
				JsonUtility.FromJsonOverwrite(trackSaveContainer.Data, track);
				tweenerTracksProperty.GetArrayElementAtIndex(i).objectReferenceValue = track;
			}

			serializedObject.ApplyModifiedProperties();
		}

		public static string GetReadableTweenerTrackName(string className)
		{
			return ObjectNames.NicifyVariableName(className.Replace(nameof(TweenerTrack), string.Empty));
		}

		#endregion

		#region Nested Classes

		[Serializable]
		private class TweenerSaveContainer
		{
			[SerializeField]
			private TweenerTrackSaveContainer[] _tweenerTrackSaveContainers;

			public TweenerTrackSaveContainer[] TweenerTrackSaveContainers => _tweenerTrackSaveContainers;

			public TweenerSaveContainer(TweenerTrackSaveContainer[] tweenerTrackSaveContainers)
			{
				_tweenerTrackSaveContainers = tweenerTrackSaveContainers;
			}
		}

		[Serializable]
		public class TweenerTrackSaveContainer
		{
			[SerializeField]
			private string _className;
			[SerializeField]
			private string _assembly;
			[SerializeField]
			private string _data;

			public string ClassName => _className;

			public string Assembly => _assembly;

			public string Data => _data;

			public TweenerTrackSaveContainer(string className, string assembly, string data)
			{
				_className = className;
				_assembly = assembly;
				_data = data;
			}
		}

		#endregion
	}
}